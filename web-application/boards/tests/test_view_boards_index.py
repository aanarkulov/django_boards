from django.test import TestCase
from django.core.urlresolvers import reverse
from django.urls import resolve

from boards.models import Board
from boards.views import BoardListView


class IndexListViewTests(TestCase):
    def setUp(self):
        self.board = Board.objects.create(name='Django', description='Django boards.')
        url = reverse('index')
        self.response = self.client.get(url)

    def test_index_view_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_index_url_resolves_index_view(self):
        view = resolve('/')
        self.assertEquals(view.func.view_class, BoardListView)

    def test_index_view_contains_link_to_topics_page(self):
        board_topics_url = reverse('board_topics', kwargs={'board_pk': self.board.pk})
        self.assertContains(self.response, 'href="{0}"'.format(board_topics_url))
