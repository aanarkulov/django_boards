from django.test import TestCase
from django.core.urlresolvers import reverse
from django.urls import resolve

from boards.models import Board
from boards.views import BoardTopicListView


class BoardTopicTests(TestCase):
    def setUp(self):
        Board.objects.create(name='Django', description='Django boards.')
        url = reverse('board_topics', kwargs={'board_pk': 1})
        self.response = self.client.get(url)

    def test_board_topics_view_success_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_board_topics_view_not_found_status_code(self):
        url = reverse('board_topics', kwargs={'board_pk': 99})
        response = self.client.get(url)
        self.assertEquals(response.status_code, 404)

    def test_board_topics_url_resolves_board_topics_view(self):
        view = resolve('/boards/1/')
        self.assertEquals(view.func.view_class, BoardTopicListView)

    def test_board_topics_view_contains_link_to_navigation_links(self):
        index_page_url = reverse('index')
        new_topic_url = reverse('new_topic', kwargs={'board_pk': 1})

        self.assertContains(self.response, 'href="{0}"'.format(index_page_url))
        self.assertContains(self.response, 'href="{0}"'.format(new_topic_url))
