from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import resolve, reverse

from boards.models import Board, Post, Topic
from boards.views import TopicPostListView


class TopicPostsTestCase(TestCase):
    def setUp(self):
        board = Board.objects.create(name='Django', description='Django board.')
        self.username = 'john'
        self.password = '123'
        user = User.objects.create_user(username=self.username, email='john@doe.com', password=self.password)
        topic = Topic.objects.create(subject='Hello, world', board=board, starter=user)
        Post.objects.create(message='Lorem ipsum dolor sit amet', topic=topic, created_by=user)
        self.url = reverse('topic_posts', kwargs={'board_pk': board.pk, 'topic_pk': topic.pk})
        self.response = self.client.get(self.url)


class LoginRequiredTopicPostsTests(TopicPostsTestCase):
    def test_redirection(self):
        login_url = reverse('login')
        response = self.client.get(self.url)
        self.assertRedirects(response, '{login_url}?next={url}'.format(login_url=login_url, url=self.url))


class TopicPostsTests(TopicPostsTestCase):
    def setUp(self):
        super().setUp()
        self.client.login(username=self.username, password=self.password)
        self.response = self.client.get(self.url)

    def test_status_code(self):
        self.assertEquals(self.response.status_code, 200)

    def test_view_function(self):
        view = resolve('/boards/1/topics/1/')
        self.assertEquals(view.func.view_class, TopicPostListView)
