from django.shortcuts import get_object_or_404, render, redirect
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import ListView, View, UpdateView, FormView

from .forms import NewTopicForm, PostForm
from .models import Board, Topic, Post


class BoardListView(ListView):
    model = Board
    context_object_name = 'boards'
    template_name = 'boards/index.html'


class BoardTopicListView(ListView):
    model = Topic
    context_object_name = 'topics'
    template_name = 'boards/board_topics.html'
    paginate_by = 20

    def get_queryset(self):
        self.board = get_object_or_404(Board, pk=self.kwargs['board_pk'])
        return self.board.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)

    def get_context_data(self, **kwargs):
        kwargs['board'] = self.board
        return super().get_context_data(**kwargs)


@method_decorator(login_required, name='dispatch')
class NewTopicFormView(FormView):
    template_name = 'boards/new_topic.html'
    form_class = NewTopicForm

    def get_context_data(self, **kwargs):
        kwargs['board'] = get_object_or_404(Board, pk=self.kwargs['board_pk'])
        return super().get_context_data(**kwargs)

    def form_valid(self, form, **kwargs):
        topic = form.save(commit=False)
        topic.board = get_object_or_404(Board, pk=self.kwargs['board_pk'])
        topic.starter = self.request.user
        topic.save()
        Post.objects.create(
            message=form.cleaned_data.get('message'),
            topic=topic,
            created_by=self.request.user
        )
        return redirect('topic_posts', board_pk=self.kwargs['board_pk'], topic_pk=topic.pk)



@method_decorator(login_required, name='dispatch')
class TopicPostListView(ListView):
    model = Post
    context_object_name = 'posts'
    template_name = 'boards/topic_posts.html'
    paginate_by = 20

    def get_queryset(self):
        self.topic = get_object_or_404(Topic, pk=self.kwargs['topic_pk'])
        return self.topic.posts.order_by('created_at')

    def get_context_data(self, **kwargs):
        session_key = 'viewed_topic_{}'.format(self.topic.pk)
        if not self.request.session.get(session_key):
            self.topic.views += 1
            self.topic.save()
            self.request.session[session_key] = True

        kwargs['topic'] = self.topic
        return super().get_context_data(**kwargs)


@method_decorator(login_required, name='dispatch')
class ReplyTopicView(View):
    def render(self, request):
        return render(request, 'boards/reply_topic.html', {'topic': self.topic, 'form': self.form})

    def get(self, request, board_pk, topic_pk):
        self.topic = get_object_or_404(Topic, board__pk=board_pk, pk=topic_pk)
        self.form = PostForm()
        return self.render(request)

    def post(self, request, board_pk, topic_pk):
        self.topic = get_object_or_404(Topic, board__pk=board_pk, pk=topic_pk)
        self.form = PostForm(request.POST)
        if self.form.is_valid():
            post = self.form.save(commit=False)
            post.topic = self.topic
            post.created_by = request.user
            post.save()

            self.topic.last_updated = timezone.now()
            self.topic.save()

            topic_url = reverse('topic_posts', kwargs={'board_pk': board_pk, 'topic_pk': topic_pk})
            print(type(topic_url))
            topic_post_url = '{url}?page={page}#{id}'.format(
                url=topic_url,
                id=post.pk,
                page=self.topic.get_page_count()
            )
            print(topic_post_url)
            return redirect(topic_post_url)
        return self.render(request)


@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
    model = Post
    fields = ('message',)
    context_object_name = 'post'
    pk_url_kwarg = 'post_pk'
    template_name = 'boards/edit_post.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(created_by=self.request.user)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.updated_by = self.request.user
        post.updated_at = timezone.now()
        post.save()
        return redirect('topic_posts', board_pk=post.topic.board.pk, topic_pk=post.topic.pk)
